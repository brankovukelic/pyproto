from distutils.core import setup

setup(
    name='pyproto',
    description='JavaScript-like prototypal object model for Python',
    long_description=open('README.rst').read(),
    version='0.0.1',
    py_modules=['pyproto'],
    author='Branko Vukelic',
    author_email='branko@brankovukelic.com',
    url='https://bitbucket.org/brankovukelic/pyproto',
    download_url='https://bitbucket.org/brankovukelic/pyproto/downloads',
    license='BSD',
    classifiers = [
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
    ],
)



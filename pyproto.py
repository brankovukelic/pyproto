from copy import copy

__all__ = ['Object']


class Object(object):
    """Base prototype for prototypal object model in Python"""

    def __init__(self, *args):
        self.__prototypes__ = args if len(args) else (Object,)
        self.__proto__ = self.__prototypes__[0]

    def __getitem__(self, name):
        return self.__getattr__(name)

    def __setitem__(self, name, val):
        return self.__setattr__(name, val)

    def __delitem__(self, name):
        return self.__delattr__(name)

    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]

        if self.__prototypes__:
            for proto in self.__prototypes__:
                v = getattr(proto, name, None)
                if v:
                    return v
            return None
        else:
            getattr(self.__proto__, name, None)

    def __setattr__(self, name, value):
        if hasattr(value, '__call__') and name != '__proto__':
            def method(*args, **kwargs):
                return value(self, *args, **kwargs)
            self.__dict__[name] = method
        else:
            self.__dict__[name] = value
        return value

    def hasattr(self, name):
        return name in self.__dict__

    def __keys__(self):
        return list(self.__map__().keys())

    def __values__(self):
        return [v for k, v in self.__items__()]

    def __iter__(self):
        return iter(self.__keys__())

    def __map__(self):
        attrs = copy(self.__dict__)
        del attrs['__proto__']
        del attrs['__prototypes__']
        return attrs

    def __items__(self):
        obj_map = self.__map__()
        return [(k, obj_map[k]) for k in list(obj_map.keys())]

    def __contains__(self, item):
        return item in self.__keys__()

    def __add__(self, obj):
        if not isinstance(obj, Object):
            raise ValueError('Cannot extend using non-Object object')

        self.__dict__.update(obj.__dict__)

    attrs = __keys__
    dict = __map__
    items = __items__

if __name__ == '__main__':
    import doctest
    doctest.testmod()
